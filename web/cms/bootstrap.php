<?php

// Two constants inform much of Craft’s ability to find resources in your project:
define('CRAFT_BASE_PATH', __DIR__);
define('CRAFT_VENDOR_PATH', CRAFT_BASE_PATH . '/../../vendor');

// Load Composer's autoloader
require_once CRAFT_VENDOR_PATH . '/autoload.php';