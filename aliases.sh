#!/bin/bash

alias composer="docker run -ti -v $(pwd):/app -w /app -u 1000 --entrypoint /usr/bin/composer craftcms/nginx:8.1-dev"
alias craft="docker-compose exec web php craft"
