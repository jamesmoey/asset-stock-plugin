# Asset Stock plugin for Craft CMS 4.x

Provide asset stock integration

![Screenshot](resources/img/plugin-logo.png)

## Requirements

This plugin requires Craft CMS 4 or later.

## Installation

To install the plugin, follow these instructions.

1. Open your terminal and go to your Craft project:

        cd /path/to/project

2. Then tell Composer to load the plugin:

        composer require /asset-stock

3. In the Control Panel, go to Settings → Plugins and click the “Install” button for Asset Stock.

## Asset Stock Overview

-Insert text here-

## Configuring Asset Stock

-Insert text here-

## Using Asset Stock

-Insert text here-

## Asset Stock Roadmap

Some things to do, and ideas for potential features:

* Release it

Brought to you by [James Moey](https://gitlab.com/jamesmoey)
