<?php

namespace moeytechnology\assetstock\controllers;

use craft\helpers\UrlHelper;
use craft\web\Controller;
use moeytechnology\assetstock\AssetStock;
use moeytechnology\assetstock\records\AccessToken;
use yii\helpers\Url;

class ConnectController extends Controller
{
    public function actionAdobe() {
        $this->requireLogin();
        $this->requireCpRequest();

        $req = $this->request;

        /** @var AssetStock $plugin */
        $plugin = $this->module;
        $accessKey = $req->getParam('access_key');

        \Craft::$app->getPlugins()->savePluginSettings($plugin, [
            'accessKey' => $accessKey
        ]);

        return $this->redirect(
            UrlHelper::urlWithParams(
                'https://adobe.oauth.moeyservices.com/dev/auth/request',
                [
                    'redirect_uri' => Url::to('/admin/actions/asset-stock/connect/adobe-capture', true),
                    'access_key' => $accessKey,
                ]
            ),
            302
        );
    }

    public function actionAdobeCapture() {
        $this->requireLogin();
        $this->requireCpRequest();

        $req = $this->request;

        $user = \Craft::$app->getUser();
        if (!$user->getIsGuest()) {
            $accessTokenRecord = AccessToken::findOne(['userId' => $user->getId()]);
            if ($accessTokenRecord === null) {
                $accessTokenRecord = new AccessToken();
                $accessTokenRecord->userId = $user->getId();
            }
            $accessTokenRecord->accessToken = $req->getParam('access_token');
            $accessTokenRecord->tokenExpiration = new \DateTime('+1 day');
            $accessTokenRecord->save();
        }

        return $this->redirect('admin/settings/plugins/asset-stock');
    }
}
