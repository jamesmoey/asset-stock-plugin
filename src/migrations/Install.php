<?php

namespace moeytechnology\assetstock\migrations;

use craft\db\Migration;
use moeytechnology\assetstock\AssetStock;

/**
 * Plugin Install.
 */
class Install extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(AssetStock::ACCESS_TOKEN_TABLE, [
            'uid' => $this->primaryKey(),
            'userId' => $this->integer()->notNull(),
            'accessToken' => $this->text()->notNull(),
            'tokenExpiration' => $this->dateTime()->notNull(),
            'dateCreated' => $this->dateTime()->notNull(),
            'dateUpdated' => $this->dateTime()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable(AssetStock::ACCESS_TOKEN_TABLE);
    }
}
