<?php

namespace moeytechnology\assetstock\records;

use craft\db\ActiveRecord;
use craft\records\User;
use moeytechnology\assetstock\AssetStock;

/**
 * Class User record.
 *
 * @property int $userId User ID
 * @property string $accessToken Access Token
 * @property \DateTime $tokenExpiration
 */
class AccessToken extends ActiveRecord
{
    public static function tableName()
    {
        return AssetStock::ACCESS_TOKEN_TABLE;
    }

    public function getUser()
    {
        return $this->hasOne(User::class, ['userId' => 'id']);
    }

    public static function primaryKey()
    {
        return ["uid"];
    }
}
