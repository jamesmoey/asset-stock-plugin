<?php
/**
 * Asset Stock plugin for Craft CMS 3.x
 *
 * Provide asset stock integration
 *
 * @link      https://gitlab.com/jamesmoey
 * @copyright Copyright (c) 2021 James Moey
 */

/**
 * @author    James Moey
 * @package   AssetStock
 * @since     1.0.0
 */
return [
    'Asset Stock plugin loaded' => 'Asset Stock plugin loaded',
];
