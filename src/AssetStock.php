<?php
/**
 * Asset Stock plugin for Craft CMS 3.x
 *
 * Provide asset stock integration
 *
 * @link      https://gitlab.com/jamesmoey
 * @copyright Copyright (c) 2021 James Moey
 */

namespace moeytechnology\assetstock;


use Craft;
use craft\base\Plugin;
use craft\services\Plugins;
use craft\events\PluginEvent;

use moeytechnology\assetstock\models\Settings;
use yii\base\Event;

/**
 * Class AssetStock
 *
 * @author    James Moey
 * @package   AssetStock
 * @since     1.0.0
 *
 */
class AssetStock extends Plugin
{
    const ACCESS_TOKEN_TABLE = '{{%assetstock_accesstoken}}';

    const EDITION_BASIC = "basic";

    // Static Properties
    // =========================================================================

    /**
     * @var AssetStock
     */
    public static $plugin;

    // Public Properties
    // =========================================================================

    /**
     * @var string
     */
    public string $schemaVersion = '1.0.1';

    /**
     * @var bool
     */
    public bool $hasCpSettings = true;

    /**
     * @var bool
     */
    public bool $hasCpSection = false;

    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        self::$plugin = $this;

        Event::on(
            Plugins::class,
            Plugins::EVENT_AFTER_INSTALL_PLUGIN,
            function (PluginEvent $event) {
                if ($event->plugin === $this) {
                }
            }
        );

        Craft::info(
            Craft::t(
                'asset-stock',
                '{name} plugin loaded',
                ['name' => $this->name]
            ),
            __METHOD__
        );
    }

    public static function editions(): array
    {
        return [
            self::EDITION_BASIC,
        ];
    }

    // Protected Methods
    // =========================================================================

    protected function createSettingsModel(): ?\craft\base\Model
    {
        return new Settings();
    }

    public function getSettingsResponse(): mixed
    {
        return \Craft::$app->controller->renderTemplate(
            'asset-stock/settings',
            [
                'settings' => $this->getSettings(),
                'plugin' => $this,
            ]
        );
    }
}
