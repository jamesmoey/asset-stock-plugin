<?php

namespace moeytechnology\assetstock\models;

use craft\base\Model;

class Settings extends Model
{
    public $accessKey;

    public function rules(): array
    {
        return [
            [['accessKey'], 'required'],
        ];
    }
}
