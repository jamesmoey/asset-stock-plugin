# Development

Bash aliases

```bash
source aliases.sh
```

Composer

```bash
composer install
```

Start Platform

```bash
docker-composer up
```

Install Craft and Plugin

```bash
craft install --username admin --site-name Test --site-url http://www.test.com --password password
craft plugin/install asset-stock
craft plugin/enable asset-stock
```

